const { crearArchivo } = require("./tabla");
const yargs = require('yargs')
.option('b',{
  alias: 'base',
  type: 'number',
  describe: "Numero para crear la tabla",
  demandOption: true,
  example: '$0 --base=5'
})
.nargs('base',1)
.check((argv, options) => {
  if (isNaN(argv.b)){
    throw new Error("No es un nro para crear una tabla de multiplicar.")
  }
  if (argv.b == 0){
    throw new Error("No debe ser 0 para crear una tabla de multiplicar.")
  }
  return true
})
.argv


crearArchivo(yargs.base)

